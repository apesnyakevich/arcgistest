//
//  AppDelegate.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var rootController: UINavigationController { return self.window?.rootViewController as! UINavigationController}

    //MARK: - Application lifecycle

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        AGCoreDataStack.setupUserStore()
        
        setupGoogleMaps()
        
        AGNavigationService.showPlaceList()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
      
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
      
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }


    //MARK: - Private
    
    private func setupGoogleMaps() {
        let gmsApiKey = "AIzaSyBN6PoHf267B801RrVE3hMbr9v_6chgVNw"
        GMSServices.provideAPIKey(gmsApiKey)
        GMSPlacesClient.provideAPIKey(gmsApiKey)
    }
}

