//
//  AGPlacesLocatorService.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

import MapKit
import ArcGIS
import SwifterSwift

class AGPlacesLocatorService: NSObject, AGPlacesLocatorServiceProtocol {
    
    private let locatorTask = AGSLocatorTask(url: URL(string: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer")!)
    private var cancelableGeocodeTask: AGSCancelable?
    
    func findFoodPlacesAt(_ location: CLLocationCoordinate2D, _ completion: @escaping AGFindFoodPlacesCompletionHandler) {
        // Cancel the last task, if it's still in progress.
        cancelableGeocodeTask?.cancel()
        
        // Configure parameters for the search task.
        let geocodeParameters = AGSGeocodeParameters()
        let center = AGSPoint.init(clLocationCoordinate2D: location)
        geocodeParameters.preferredSearchLocation = center
        geocodeParameters.maxResults = 20
        geocodeParameters.resultAttributeNames.append(contentsOf: [AGPlaceAttributes.fullAddress, AGPlaceAttributes.name, AGPlaceAttributes.phone, AGPlaceAttributes.url, AGPlaceAttributes.distance])
        cancelableGeocodeTask = locatorTask.geocode(withSearchText: "Food", parameters: geocodeParameters) {(results: [AGSGeocodeResult]?, error: Error?) -> Void in
            
            guard error == nil else {
                debugPrint("Geocode error", error!.localizedDescription)
                completion(nil)
                return
            }
            
            guard let results = results, results.count > 0 else {
                debugPrint("No places found")
                completion(nil)
                return
            }
            
            var places: [Any] = []
            for result in results {
                guard let placeLocation = result.displayLocation else { continue }
                guard let placeDetails = result.attributes else { continue }
                
                var placeInfo = [String:Any]()
                
                placeInfo[AGPlaceAttributes.latitude] = placeLocation.toCLLocationCoordinate2D().latitude
                placeInfo[AGPlaceAttributes.longitude] = placeLocation.toCLLocationCoordinate2D().longitude
                
                placeInfo += placeDetails
                
                debugPrint(placeInfo)
                places.append(placeInfo)
            }
            
            completion(places)
        }
    }
}
