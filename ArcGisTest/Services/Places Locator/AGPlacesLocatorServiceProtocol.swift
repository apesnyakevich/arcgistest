//
//  AGPlacesLocatorServiceProtocol.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

import MapKit

public struct AGPlaceAttributes {
    static let fullAddress = "Place_addr"
    static let name = "PlaceName"
    static let phone = "Phone"
    static let url = "URL"
    static let distance = "Distance"
    static let latitude = "Latitude"
    static let longitude = "Longitude"
}

public typealias AGFindFoodPlacesCompletionHandler = ((_ foodPlaces: [Any]?) -> Void)

protocol AGPlacesLocatorServiceProtocol {
    
    func findFoodPlacesAt(_ location: CLLocationCoordinate2D, _ completion: @escaping AGFindFoodPlacesCompletionHandler)
}
