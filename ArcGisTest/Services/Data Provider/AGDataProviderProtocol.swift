//
//  AGDataProviderProtocol.swift
//  ArcGisTest
//
//  Created by Andrew on 26/03/2018.
//  Copyright © 2019 Andrew. All rights reserved.
//

import Foundation
import MagicalRecord

protocol AGDataProviderProtocol {
    
    //MARK: - Context
    func mainContext() -> NSManagedObjectContext
    func childContextWithParent(_ parent: NSManagedObjectContext?) -> NSManagedObjectContext
    func saveMainContextWithCompletion(_ completion: @escaping (Bool, Error?) -> Swift.Void?)
    func resetMainContext()
    func saveContext(_ context: NSManagedObjectContext?, completion:((Bool, Error?) -> Swift.Void)?)
    func resetContext(_ context: NSManagedObjectContext)
    
    //MARK: - Food Places
    func allFoodPlaces() -> [AGFoodPlaceEntity]
    
    func insertFoodPlaces(places: [Any], context: NSManagedObjectContext?, completion: @escaping (_ completed: Bool)->Void)
    
    func deleteAllFoodPlaces()
}
