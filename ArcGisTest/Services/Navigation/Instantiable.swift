//
//  Instantiable.swift
//  ArcGisTest
//
//  Created by Andrew on 11/26/18.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

enum Storyboard: String, CaseIterable {
    case main = "MainFlow"
}

extension Storyboard {
    var viewControllers: [String] {
        switch self {
        case .main:
            return [AGPlaceListViewController.name,
                    AGPlaceDetailsViewController.name]
        }
    }
}

extension Storyboard {
    static func value(byViewController viewController: UIViewController.Type) -> Storyboard {
        return value(byViewController: viewController.name)
    }
    
    static func value(byViewController viewControllerName: String) -> Storyboard {
        for storyboard in Storyboard.allCases
            where storyboard.viewControllers.contains(viewControllerName) {
                return storyboard
        }
        
        fatalError("ViewController: \"\(viewControllerName)\" is not defined in Storyboard enum")
    }
}

protocol Instantiable: class {}

extension Instantiable where Self: UIViewController {
    static var instantiate: Self {
        return UIViewController.instantiate(vcName: self.name)
    }
}

extension UIViewController: Instantiable {
    static var name: String {
        return String(describing: self)
    }
    
    static func instantiate<Instantiable: UIViewController>(vcName: String) -> Instantiable {
        let storyboard = UIStoryboard(name: Storyboard.value(byViewController: vcName).rawValue,
                                      bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: vcName) as? Instantiable else {
            fatalError("ViewController: \"\(vcName)\" incorrect cast to \(Instantiable.self)")
        }
        
        return viewController
    }
}
