//
//  AGScreenAssembler+InitialFlow.swift
//  ArcGisTest
//
//  Created by Andrew on 31/01/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import Foundation
import Swinject

extension AGScreenAssembler {
  
    class func placeListScreen(container: Container) -> AGPlaceListViewController {
        let placeListViewController = AGPlaceListViewController.instantiate
        
        let dataProvider = container.resolve(AGDataProviderProtocol.self)
        let placesLocator = container.resolve(AGPlacesLocatorServiceProtocol.self)
        
        let model = AGPlaceListModel()
        let viewModel = AGPlaceListViewModel(view: placeListViewController, model: model, dataProvider: dataProvider, placesLocatorService: placesLocator)
        placeListViewController.viewModel = viewModel
        
        return placeListViewController
    }
    
    class func placeDetailsScreen(container: Container, place: AGFoodPlaceEntity?) -> AGPlaceDetailsViewController {
        let placeDetailsViewController = AGPlaceDetailsViewController.instantiate
        
        let model = AGPlaceDetailsModel()
        let viewModel = AGPlaceDetailsViewModel(view: placeDetailsViewController, model: model)
        viewModel.place = place
        placeDetailsViewController.viewModel = viewModel
        
        return placeDetailsViewController
    }
    
}
