//
//  SwinjectStoryboard.swift
//  ArcGisTest
//
//  Created by Andrew on 27.03.2018.
//  Copyright © 2019 Andrew. All rights reserved.
//

import SwinjectStoryboard

extension SwinjectStoryboard {
    @objc class func setup() {
        defaultContainer.register(AGDataProviderProtocol.self) { _ in AGDataProvider() }
        defaultContainer.register(AGPlacesLocatorServiceProtocol.self) { _ in AGPlacesLocatorService() }
    }
}
